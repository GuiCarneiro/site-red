FactoryGirl.define do
  factory :post do
    title        "Título"
    text         { Faker::Lorem.paragraph }
    header       { Faker::Placeholdit.image("360x168") }
    team_member
    tag
  end
end

