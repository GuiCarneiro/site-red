FactoryGirl.define do
  factory :tag do
    sequence(:title) { |i| "Tag ##{i}" }
  end
end

