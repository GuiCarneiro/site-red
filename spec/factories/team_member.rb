FactoryGirl.define do
  factory :team_member do
    name         { Faker::Name.name }
    facebook     "https://fb.com"
    linkedin     "https://linkedin.com"
    twitter      "https://twitter.com"
    github       "https://github.com"
    behance      "https://behance.com"
    photo        { Faker::Placeholdit.image("100x100") }
    job          "Developer"
  end
end

