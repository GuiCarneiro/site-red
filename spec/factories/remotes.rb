FactoryGirl.define do
  factory :remote do
    name "MyString"
    email "MyString"
    remote_type "MyString"
    tecnologies "MyText"
    github "MyString"
    behance "MyString"
    links "MyText"
    about "MyText"
    how_you_met_red "MyString"
  end
end
