require 'rails_helper'

RSpec.describe PostView, type: :model do
  context "associations" do
    it { should belong_to(:post) }
  end
end
