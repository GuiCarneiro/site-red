require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:post) { create(:post) }

  it "should have a valid factory" do
    expect(build(:post)).to be_valid
  end

  context "associations" do
    it { should belong_to :team_member }
    it { should belong_to(:tag) }
    it { should have_many(:post_views) }
    it { should have_many(:comments) }
  end

  describe "#add_view" do
    it "should create a post view" do
      expect { post.add_view }.to change { post.post_views.count }.by(1)
    end
  end

  describe ".popular" do
    pending "Should return popular posts"
  end
end
