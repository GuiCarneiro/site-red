require 'rails_helper'

RSpec.describe Lead, type: :model do
  context "associations" do
    it { should have_many(:comments) }
  end
end
