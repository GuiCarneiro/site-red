require 'rails_helper'

RSpec.describe TeamMember, type: :model do
  context "associations" do
    it { should have_many :posts }
  end

  context "scopes" do
    describe ".active" do
      before :each do
        @team_member_1 = create(:team_member, is_active: true)
        @team_member_2 = create(:team_member, is_active: false)
      end

      it "should return only active team member" do
        expect(TeamMember.active).to     include(@team_member_1)
        expect(TeamMember.active).not_to include(@team_member_2)
      end
    end
  end
end
