require "administrate/base_dashboard"

class PostDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    title: Field::String,
    keywords: Field::String,
    text: MCEField,
    team_member: Field::BelongsTo,
    tag: Field::BelongsTo,
    is_visible: Field::Boolean,
    header: CarrierwaveField,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    meta_description: Field::String,
    meta_title: Field::String,
    language: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :language,
    :is_visible,
    :created_at,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :title,
    :is_visible,
    :text,
    :team_member,
    :tag,
    :header,
    :keywords,
    :meta_title,
    :meta_description,
    :language,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :meta_title,
    :meta_description,
    :text,
    :header,
    :team_member,
    :tag,
    :is_visible,
    :keywords,
    :language,
  ].freeze

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(user)
  #   "User ##{user.id}"
  # end

  def display_resource(post)
    "#{post.title}"
  end
end
