module ApplicationHelper
  def clean text
    result = strip_tags text

    if result.length > 240
      result = result[0..239] + "..."
    end

    return result
  end

  def subscribed
    if cookies[:subscribed]
      return "subscribed"
    else
      return "not-subscribed"
    end
  end

  def toastr(message, duration)
    render inline: javascript_tag("Materialize.toast('#{message}', #{duration})")
  end
end
