var testimonialsInitialSlot = 0;
var initialSlot = 0;

$( document ).ready(function() {
  $(".testimonials-col").each(function( index ) {
    if($(this).data("id") < initialSlot || $(this).data("id") > initialSlot + 1){
      $(this).hide();
    }
  });

  for (var i = 0; i < Math.ceil($(".testimonials-col").length / 2); i++) {
    if(i == 0){
      $(".indicators-row").append("<div class='indicator active' data-id='" + i + "'></div>");
    }
    else{
      $(".indicators-row").append("<div class='indicator' data-id='" + i + "'></div>");
    }
  }

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && Math.ceil($(".testimonials-col").length > 2) ){
    setInterval(function() {
      nextSlideTestimonial();
    }, 7000);
  }
});

function nextSlideTestimonial(){
  testimonialsInitialSlot = testimonialsInitialSlot + 2;

  if(testimonialsInitialSlot + 2 > $(".testimonials-col").length){
    testimonialsInitialSlot = 0;
  }

  $(".testimonials-col").each(function( index ) {
    if($(this).data("id") < testimonialsInitialSlot || $(this).data("id") > testimonialsInitialSlot + 1){
      $(this).addClass("animated fadeOutLeft");
    }
  });

  setTimeout(function() {
    $(".testimonials-col").each(function( index ) {
      if($(this).data("id") >= testimonialsInitialSlot && $(this).data("id") <= testimonialsInitialSlot + 1){
        $(this).addClass("animated fadeInRight");
        $(this).show();
      }
      else{
        $(this).removeClass("animated fadeOutRight fadeOutLeft fadeInRight fadeInLeft");
        $(this).hide();
      }
    });

    $(".indicator").each(function(index) {
      if(index != Math.ceil(testimonialsInitialSlot / 2)){
        $(this).removeClass("active");
      }
      else{
        $(this).addClass("active");
      }
    });
  }, 300);
}

