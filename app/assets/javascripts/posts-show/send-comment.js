$("#post-comment").on('submit',function(data){
  var target = data.target;
  var valid = true;

  if($("#text").val() == undefined || $("#text").val() == ""){
    Materialize.toast('Não esqueça de preencher o seu comentário', 3000);
    valid = false;
  }

  if($("#email").val() == undefined || $("#email").val() == ""){
    Materialize.toast('Não esqueça de preencher o seu email', 3000);
    valid = false;
  }

  if($("#name").val() == undefined || $("#name").val() == ""){
    Materialize.toast('Não esqueça de preencher o seu nome', 3000);
    valid = false;
  }

  if(!valid){
    data.preventDefault();
  }
})
