$( document ).ready(function() {
  var popup_showing = false

  if($("#posts-show.not-subscribed").length) {
    setTimeout(function(){
      $("#subscribe-popup").addClass("visible");
    }, 15000);
  }
});

function closePopup(){
  $("#subscribe-popup").removeClass("visible");
}

function submitSubscribe(){
  console.log('Enter Working')
}


$("#submit-subscribe").submit(function(e){
  e.preventDefault();
  var email = $("#subscription-email").val();
  $("#subscription-form").hide();
  $("#subscribe-loading").show();

  $.ajax({
    type: "POST",
    url: '/subscribers',
    data: {
      email: email
    },
    success: function(){
      $("#subscribe-loading").hide();
      $("#success-message").show();

      setTimeout(function(){
        $("#subscribe-popup").removeClass("visible");
      }, 2000);
    }
  });
});
