var blog_actual_page = 1;

$( document ).ready(function() {
  if ($("#posts-index").size() > 0)
  {
    loadMore();
  }
});


function loadMore(){
  blog_actual_page = blog_actual_page + 1;
  blog_actual_filter = $("#list-of-posts").data( "filter" )
  var filter = ""

  $(".load-more").hide();

  $.get( "/blog?page=" + blog_actual_page + "?filter=" + blog_actual_filter, function( data ) {
    if(data.length > 0){
      $(".load-more").show();

      $.each(data, function( index, value ) {
        value.text = value.text.replace(/<(?:.|\n)*?>/gm, '').substring(0,239) + "...";
        value.created_at = moment(value.created_at).format('DD/MM/YYYY [às] hh:mm');

        $(".main-card").append(
          "<div class='post row'>"+
            "<div class='col l6 photo-col'>" +
              "<a href='/posts/" + value.id + "'>" +
                "<img src='" + value.header.url + "'>" +
              "</a>" +
            "</div>" +
            "<div class='col l6 text-col'>" +
              "<h5 class='tag-title'>" +
                "<a href='?filter=" + value.tag.id + "'>" +
                  value.tag.title +
                "</a>" +
              "</h5>" +
              "<h4 class='post-title'>" +
                "<a href='/posts/" + value.id + "'>" +
                  value.title +
                "</a>" +
              "</h4>" +
              "<p class='description roboto'>" +
                value.text +
              "</p>" +
              "<a class='see-more' href='/posts/" + value.id +"'>" +
                "Ver mais" +
              "</a>" +
              "<div class='row user-row'>" +
                "<div class='col s3 writer-image' style='background-image: url(" + value.team_member.photo.url + ");'>" +
                "</div>" +
                "<div class='col s9 post-data'>" +
                  "<span class='writer-name'>" +
                    value.team_member.name +
                  "</span>" +
                  "<br>" +
                  "<span class='post-date'>" +
                    value.created_at +
                  "</span>" +
                "</div>" +
              "</div>" +
            "</div>" +
          "</div>"
        )
      });
    }
  });
}
