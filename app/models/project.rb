class Project < ApplicationRecord
  after_commit :send_notice
  before_save :set_location

  def send_notice
    client = Slack::Web::Client.new

    channels = client.channels_list.channels

    client.chat_postMessage(channel: '#sociedade', text: 'Novo Projeto! Olhe o sistema administrativo: https://www.red-rocket.tech/admin/projects', as_user: true)
  end

  def set_location
    self.location = I18n.locale
  end
end
