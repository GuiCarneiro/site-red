class Post < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :team_member
  belongs_to :tag, :counter_cache => true
  has_many :post_views
  has_many :comments

  mount_uploader :header, PhotoUploader

  def add_view
    PostView.create(post_id: self.id)
  end

  def self.popular
    joins(:post_views)
    .where("post_views.created_at > ? AND post_views.created_at < ?", Time.now.beginning_of_month, Time.now.end_of_month)
    .group('posts.id')
    .order("count(posts.id) desc")
  end

  def self.visible
    where(is_visible: true)
  end

  def self.localized
    where(language: I18n.locale)
  end
end
