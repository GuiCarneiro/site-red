class TeamMember < ApplicationRecord
  scope :active, -> { where(is_active: true) }

  mount_uploader :photo, PhotoUploader
  has_many :posts
end
