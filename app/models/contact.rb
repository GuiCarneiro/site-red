class Contact < ApplicationRecord
  after_create :send_notice

  def send_notice
    client = Slack::Web::Client.new

    channels = client.channels_list.channels

    client.chat_postMessage(channel: '#sociedade', text: 'Novo Contato! Olhe o sistema administrativo: https://www.red-rocket.tech/admin/contacts', as_user: true)
  end
end
