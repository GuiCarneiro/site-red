class Mailchimp::Base
  def initialize()
    @gibbon = Gibbon::Request.new(api_key: ENV["MAILCHIMP_KEY"])
  end

  def add_to_subscription_list(email)
    begin
      @gibbon.lists(ENV["SUBSCRIPTION_LIST_ID"])
      .members
      .create(
        body: {
          email_address: email,
          status: "subscribed",
        })
    rescue
    end
  end
end
