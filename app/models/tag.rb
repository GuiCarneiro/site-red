class Tag < ApplicationRecord
  has_many :posts, :counter_cache => true

  def self.popular
    where("posts_count > 0").order('posts_count DESC')
  end
end
