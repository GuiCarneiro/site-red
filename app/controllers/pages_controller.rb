class PagesController < ApplicationController
  before_action :prepare_meta_tags, if: "request.get?"

  def home
    @posts = Post.includes(:team_member, :tag).localized.visible.last(3)
  end

  private
    def domain_cache_directory
      Rails.root.join("public", request.domain)
    end
end
