class SubscribersController < ApplicationController
  def create
    mailchimp = Mailchimp::Base.new
    mailchimp.add_to_subscription_list(params[:email])
    cookies[:subscribed] = true

    render json: {}.to_json(), status: :ok
  end
end
