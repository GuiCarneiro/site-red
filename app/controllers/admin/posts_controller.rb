module Admin
  class PostsController < Admin::ApplicationController
    # Define a custom finder by overriding the `find_resource` method:
    def find_resource(param)
      Post.find_by!(slug: param)
    end
  end
end
