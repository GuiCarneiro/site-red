class ProjectsController < ApplicationController
  before_action :prepare_meta_tags, if: "request.get?"

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to root_path, notice: t("projects.create.success") }
      else
        format.html { redirect_to root_path }
      end
    end
  end

  private
    def project_params
      params.permit(:name, :user_name, :project_type, :budget, :timing, :user_email, :user_phone, :location)
    end
end
