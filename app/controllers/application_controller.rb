class ApplicationController < ActionController::Base
  include Clearance::Controller
  before_action :set_locale

  def set_locale
    if !request.env['HTTP_ACCEPT_LANGUAGE'].blank?
      I18n.locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    end
  end

  def prepare_meta_tags(options={})
    defaults = {
      site: t("metatags.site"),
      title: t("metatags.title"),
      image: t("metatags.image"),
      description: t("metatags.description"),
      og: {
        url: request.url,
        site: t("metatags.og.site"),
        title: t("metatags.og.title"),
        image: t("metatags.og.image"),
        description: t("metatags.og.description"),
        type: 'website'
      },
      twitter:{
        card: "summary",
        site: "@red_rckt",
        image: t("metatags.twitter.image"),
        title: t("metatags.twitter.title"),
        description: t("metatags.twitter.description"),
      }
    }

    options.reverse_merge!(defaults)
    set_meta_tags options
  end
end
