class PostsController < ApplicationController
  before_action :load_popular_posts
  before_action :prepare_post_tags, only: [:show], if: "request.get?"
  before_action :prepare_meta_tags, only: [:index], if: "request.get?"

  def index
    @tags = Tag.popular
    @filter = params[:filter]

    if params[:filter].blank?
      @posts = Post.order(created_at: :desc)
      .visible
      .localized
      .includes(:team_member, :tag)
      .page(params[:page]).per(5)
    else
      @posts = Post.where(tag_id: params[:filter])
      .visible
      .localized
      .order(created_at: :desc)
      .includes(:team_member, :tag)
      .page(params[:page]).per(5)
    end

    respond_to do |format|
      format.json { render json: @posts.to_json(include: [:team_member, :tag]) }
      format.html { render :index }
    end
  end

  def show
    @post = Post.friendly.find(params[:id])

    @comments = @post.comments.includes(:lead)
    @post.add_view
  end

  private
    def load_popular_posts
      @popular_posts = Post.popular
                           .localized
                           .visible
                           .limit(3)
    end

    def prepare_post_tags(options={})
      post = Post.friendly.find(params[:id])

      defaults = {
        site:        "RedRocket - Blog",
        title:       post.meta_title,
        image:       post.header.url,
        description: post.meta_description,
        keywords: post.keywords || "",
        og: {
          url: request.url,
          site: "RedRocket - Blog",
          title: post.meta_title,
          image: post.header.url,
          description: post.meta_description,
          type: 'website'
        },
        twitter:{
          card: "summary",
          site: "@red_rckt",
          image: post.header.url,
          title: post.meta_title,
          description: post.meta_description
        }
      }

      options.reverse_merge!(defaults)
      set_meta_tags options
    end
end
