class CommentsController < ApplicationController
  def create
    @lead = Lead.find_by(email: params[:email])

    if verify_recaptcha(model: @lead)
      if @lead.nil?
        @lead = Lead.create(email: params[:email], name: params[:name])
      end

      @comment = Comment.new(post_id: params[:post_id], lead_id: @lead.id, text: params[:text])
      respond_to do |format|
        if @comment.save
          format.html { redirect_to :back, notice: t("comment.success") }
        else
          format.html { redirect_to :back }
        end
      end
    else
      redirect_to :back
    end
  end
end
