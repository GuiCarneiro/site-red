class RemotesController < ApplicationController
  before_action :prepare_meta_tags, if: "request.get?"

  def new
    @remote = Remote.new
  end

  def create
    @remote = Remote.new(remote_params)

    respond_to do |format|
      if @remote.save
        format.html { redirect_to root_path, notice: t("remotes.create.success") }
      else
        format.html { redirect_to root_path }
      end
    end
  end

  private
    def remote_params
      params.permit(:name, :email, :remote_type, :tecnologies, :github, :behance, :links, :about, :how_you_met_red)
    end
end
