source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
gem 'ionicons-rails'

# Mailchimp API
gem 'gibbon'

gem 'canonical-rails', github: 'jumph4x/canonical-rails'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use slim template in views.
gem 'slim-rails'
gem 'meta-tags'
# Nice icons
gem 'bourbon'
gem 'neat'
gem 'momentjs-rails'
gem 'font-awesome-rails'
gem 'net-ssh'
gem "fog"
gem "mime-types"
gem 'mini_magick'
gem 'carrierwave'
gem 'kaminari'
gem "administrate", "~> 0.3.0"
gem "clearance"
gem 'rollbar'
gem 'friendly_id', '~> 5.1.0'
gem 'sitemap_generator'

# Figaro - For ENV Variables
gem "figaro"

# Rack SSL
gem 'rack-ssl'

# Slack
gem 'slack-ruby-client'

#I18n for JS
gem "i18n-js"

#Captcha
gem "recaptcha", require: "recaptcha/rails"

group :development, :test do
  gem "rspec-rails"
  gem "rails-controller-testing"
  gem "factory_girl_rails"
	gem "shoulda-matchers"
  gem "faker"
  gem "pry"
  gem "pry-byebug"
  gem "pry-rails"
  gem "pry-remote"
  gem "pry-stack_explorer"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'bullet'
end

group :production do
  gem 'heroku-deflater'
  gem 'dalli'
  #gem 'kgio'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
