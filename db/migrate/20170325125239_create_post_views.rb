class CreatePostViews < ActiveRecord::Migration[5.0]
  def change
    create_table :post_views do |t|
      t.integer :post_id, index: true

      t.timestamps
    end
  end
end
