class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
      t.string :client_name
      t.string :client_photo
      t.text :text
      t.string :project_name

      t.timestamps
    end
  end
end
