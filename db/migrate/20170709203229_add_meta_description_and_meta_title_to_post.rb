class AddMetaDescriptionAndMetaTitleToPost < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :meta_description, :text
    add_column :posts, :meta_title, :string
  end
end
