class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :text
      t.integer :team_member_id, index: true
      t.integer :tag_id, index: true
      t.boolean :is_visible, default: false
      t.string :header
      t.timestamps
    end
  end
end
