class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :year
      t.text :description
      t.string :photo
      t.integer :web_or_app

      t.timestamps
    end
  end
end
