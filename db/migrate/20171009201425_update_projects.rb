class UpdateProjects < ActiveRecord::Migration[5.0]
  def change
    #removing old columns
    remove_column :projects, :year, :integer
    remove_column :projects, :description, :text
    remove_column :projects, :photo, :text
    remove_column :projects, :web_or_app, :string
    remove_column :projects, :behance_url, :string
    remove_column :projects, :website_url, :string
    remove_column :projects, :apple_url, :string
    remove_column :projects, :android_url, :string

    #adding new columns
    add_column :projects, :user_name, :string
    add_column :projects, :user_phone, :string
    add_column :projects, :user_email, :string
    add_column :projects, :project_type, :string
    add_column :projects, :budget, :string
    add_column :projects, :timing, :string
  end
end
