class CreateTeamMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :team_members do |t|
      t.string :name
      t.string :facebook
      t.string :linkedin
      t.string :twitter
      t.string :github
      t.string :behance
      t.string :photo
      t.string :job
      t.boolean :is_active, default: true

      t.timestamps
    end
  end
end
