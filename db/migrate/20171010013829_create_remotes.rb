class CreateRemotes < ActiveRecord::Migration[5.0]
  def change
    create_table :remotes do |t|
      t.string :name
      t.string :email
      t.string :remote_type
      t.text :tecnologies
      t.string :github
      t.string :behance
      t.text :links
      t.text :about
      t.string :how_you_met_red

      t.timestamps
    end
  end
end
