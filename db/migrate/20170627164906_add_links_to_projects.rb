class AddLinksToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :behance_url, :string
    add_column :projects, :website_url, :string
    add_column :projects, :apple_url, :string
    add_column :projects, :android_url, :string
  end
end
