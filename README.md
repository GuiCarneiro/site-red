# Dependencies
* Ruby 2.3.1
* Rails 5.0.2
* PostgreSQL 9.5

# Testing suite
* rspec 3.5
