Rails.application.routes.draw do
  resources :contacts, only: [:create]

  resources :comments, only: [:create]


  get '/blog' => 'posts#index', as: :posts
  get '/blog/:id' => 'posts#show', as: :post

  resources :subscribers, only: [:create]
  resources :projects, only: [:new, :create]
  resources :remotes, only: [:new, :create]

  namespace :admin do
    resources :users
    resources :team_members
    resources :projects
    resources :contacts
    resources :posts
    resources :tags
    resources :leads
    resources :comments
    resources :remotes

    root to: "team_members#index"
  end

  root 'pages#home'
end
