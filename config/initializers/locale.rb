# Set default locale to something other than :en
I18n.enforce_available_locales = true
I18n.available_locales = ["pt", "en"]

# Where the I18n library should search for translation files
I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

